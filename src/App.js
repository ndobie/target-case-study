import React, { Component } from 'react'
import { css, media, presets } from 'glamor'

import rawItemData from './item-data.json'
import ProductHeader from './components/ProductHeader'
import ProductDetails from './components/ProductDetails'
import ReviewsSummary from './components/ReviewsSummary'

const itemData = rawItemData.CatalogEntryView[0]

console.log(itemData)

const productHeaderData = {
  title: itemData.title,
  images: itemData.Images[0].PrimaryImage.concat(itemData.Images[0].AlternateImages).map(image => image.image)
}

const productDetailsData = {
  price: itemData.Offers[0].OfferPrice[0],
  promoItems: itemData.Promotions,
  purchaseCode: parseInt(itemData.purchasingChannelCode, 10),
  productHighlights: itemData.ItemDescription[0].features
}

const reviewsSummaryData = {
  overallStars: parseInt(itemData.CustomerReview[0].consolidatedOverallRating, 10),
  totalReviews: parseInt(itemData.CustomerReview[0].totalReviews, 10),
  bestProReview: itemData.CustomerReview[0].Pro[0],
  bestConReview: itemData.CustomerReview[0].Con[0]
}

reviewsSummaryData.bestProReview.overallRating = parseInt(reviewsSummaryData.bestProReview.overallRating, 10)
reviewsSummaryData.bestConReview.overallRating = parseInt(reviewsSummaryData.bestConReview.overallRating, 10)
reviewsSummaryData.bestProReview.datePosted = new Date(reviewsSummaryData.bestProReview.datePosted)
reviewsSummaryData.bestConReview.datePosted = new Date(reviewsSummaryData.bestConReview.datePosted)

const appStyles = css(
  {
    position: 'relative',
    padding: '2rem'
  },
  media(presets.desktop, {
    maxWidth: '70vw',
    margin: '0 auto'
  })
)

export default class App extends Component {
  constructor (...args) {
    super(...args)

    this.state = {quanity: 1, activeImageIndex: 0}

    this.onUpdateQuanity = (quanity) => {
      if (quanity < 1) {
        this.setState({quanity: 1})
      } else if (quanity > 10) {
        this.setState({quanity: 10})
      } else {
        this.setState({quanity})
      }
    }

    this.onUpdateActiveImage = (activeImageIndex) => this.setState({activeImageIndex})
  }


  render() {
    return (
      <div {...appStyles}>
        <ProductHeader
          {...productHeaderData}
          activeImageIndex={this.state.activeImageIndex}
          onUpdateActiveImage={this.onUpdateActiveImage}
          onViewLargerImage={(index) => alert(`Larger Image: ${productHeaderData.images[index]}`)} />
        <ProductDetails
          {...productDetailsData}
          quanity={this.state.quanity}
          onAddToCart={() => alert('added to cart')}
          onFindInAStore={() => alert('find in a store')}
          onPickUpInStore={() => alert('pick up in store')}
          onAddToRegistry={() => alert('onAddToRegistry')}
          onAddToList={() => alert('onAddToList')}
          onShare={() => alert('onShare')}
          onUpdateQuanity={this.onUpdateQuanity} />
        <ReviewsSummary {...reviewsSummaryData} />
      </div>
    );
  }
}
