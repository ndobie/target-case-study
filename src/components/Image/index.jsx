import React from 'react'
import T from 'prop-types'
import { css } from 'glamor'

const baseFigureStyles = css({
  backgroundPosition: 'center center',
  backgroundRepeat: 'no-repeat',
  backgroundSizing: 'contain',
  borderRadius: 3,
  overflow: 'hidden',
  padding: 0,
  margin: 0,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
})

const imageStyles = css({
  maxWidth: '100%',
  maxHeight: '100%'
})

const Image = ({ src, width, height, outline }) => {
  const figureStyles = css({
    width,
    height,
    border: outline ? '1px solid #CCC' : undefined
  })

  return <figure {...baseFigureStyles} {...figureStyles}><img {...imageStyles} src={src} /></figure>
}

Image.propTypes = {
  src: T.string.isRequired,
  width: T.oneOfType([T.number, T.string]).isRequired,
  height: T.oneOfType([T.number, T.string]).isRequired,
  outline: T.bool
}

export default Image
