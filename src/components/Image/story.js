import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import Image from './index'

const src = 'http://target.scene7.com/is/image/Target/14263758'

storiesOf('elements.Image', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false
  }))
  .add('thumnail', () => (
    <Image src={src} width={100} height={100}  />
  ))
  .add('thumbnail with outline', () => (
    <Image src={src} width={100} height={100} outline />
  ))
  .add('large', () => (
    <Image src={src} width={400} height={400} />
  ))
