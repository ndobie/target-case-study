import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import Stars from './index'

storiesOf('elements.Stars', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false
  }))
  .add('no stars', () => (
    <Stars num={0} />
  ))
  .add('one stars', () => (
    <Stars num={1} />
  ))
  .add('two stars', () => (
    <Stars num={2} />
  ))
  .add('three stars', () => (
    <Stars num={3} />
  ))
  .add('four stars', () => (
    <Stars num={4} />
  ))
  .add('five stars', () => (
    <Stars num={5} />
  ))
  .add('five stars large', () => (
    <Stars num={5} large />
  ))
