import React from 'react'
import T from 'prop-types'
import { css, lastChild, nthChild } from 'glamor'

const starsContainerStyles = css({
  display: 'inline'
})

const starStyles = css(
  {
    marginRight: '0.25rem',
    color: '#CB0000'
  },
  lastChild({
    marginRight: 0
  }),
)

const Stars = ({num, large}) => {
  const starsContainerSelectedStyles = css({
    fontSize: large ? '1.5rem' : undefined
  })

  const starSelectedStyles = css(nthChild(`n+${num + 1}`, {
    color: '#CCC'
  }))

  return (
    <div {...starsContainerStyles} {...starsContainerSelectedStyles}>
      <span {...starStyles} {...starSelectedStyles}>
        <i className="icon-">star</i>
      </span>
      <span {...starStyles} {...starSelectedStyles}>
        <i className="icon-">star</i>
      </span>
      <span {...starStyles} {...starSelectedStyles}>
        <i className="icon-">star</i>
      </span>
      <span {...starStyles} {...starSelectedStyles}>
        <i className="icon-">star</i>
      </span>
      <span {...starStyles} {...starSelectedStyles}>
        <i className="icon-">star</i>
      </span>
    </div>
  )
}

Stars.propTypes = {
  num: T.number.isRequired,
  large: T.bool
}

Stars.defaultProps = {
  large: false
}

export default Stars
