import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import Review from './index'

storiesOf('module.Review', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 300
  }))
  .add('default', () => (
    <Review
      stars={4}
      shortDescription='Amazing Product'
      longDescription='One of the best products on the market today! Love it.'
      date={new Date(2017, 2, 13)}
      authorName='Nick'
      authorLink='http://target.com'
      />
  ))
