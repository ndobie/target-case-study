import React from 'react'
import T from 'prop-types'
import { css } from 'glamor'
import moment from 'moment'

import Stars from '../Stars'
import Heading, { H3 } from '../Heading'

const reviewStyles = css({
  marginTop: 0,
  paddingTop: 0,
  marginBottom: '1rem',
  marginTop: 0
})

const screenNameStyles = css({
  color: 'blue',
  cursor: 'pointer',
  textDecoration: 'underline'
})

const Review = ({overallRating, title, review, datePosted, screenName}) => (
  <div>
    <div><Stars num={overallRating} /></div>
    <Heading type={H3}>{title}</Heading>
    <p {...reviewStyles}>{review}</p>
    <div>
      <a {...screenNameStyles} onClick={() => alert(`${screenName}'s profile`)}>{screenName}</a>
      &nbsp;
      <time dateTime={datePosted.toISOString}>{moment(datePosted).format('LL')}</time>
    </div>
  </div>
)

Review.propTypes = {
  overallRating: T.number.isRequired,
  title: T.string.isRequired,
  review: T.string.isRequired,
  datePosted: T.instanceOf(Date).isRequired,
  screenName: T.string.isRequired
}

export default Review
