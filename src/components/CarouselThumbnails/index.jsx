import React from 'react'
import T from 'prop-types'

import Button, { LINK } from '../Button'
import Image from '../Image'
import Flex from '../Flex'

const CarouselThumbnails = ({images, activeIndex, onUpdateIndex}) => {

  const lastIndex = images.length - 1
  const prevIndex = activeIndex === 0 ? lastIndex : activeIndex - 1
  const nextIndex = activeIndex === lastIndex ? 0 : activeIndex + 1

  return (
    <Flex container align='center'>
      <Flex />
      <Flex columns={0}>
        <Button
          type={LINK}
          onClick={() => onUpdateIndex(prevIndex)} >
            <i className='icon-' style={{fontSize: '3rem'}}>caret_left</i>
        </Button>
      </Flex>
      <Flex columns={0}>
        <div onClick={() => onUpdateIndex(prevIndex)}><Image width={100} height={100} src={images[prevIndex]} /></div>
      </Flex>
      <Flex columns={0}>
        <Image width={100} height={100} src={images[activeIndex]} outline/>
      </Flex>
      <Flex columns={0}>
        <div onClick={() => onUpdateIndex(nextIndex)}><Image width={100} height={100} src={images[nextIndex]} /></div>
      </Flex>
      <Flex columns={0}>
        <Button
          type={LINK}
          onClick={() => onUpdateIndex(nextIndex)} >
            <i className='icon-' style={{fontSize: '3rem'}}>caret_right</i>
        </Button>
      </Flex>
      <Flex />
    </Flex>
  )
}

CarouselThumbnails.propTypes = {
  images: T.arrayOf(T.string).isRequired,
  activeIndex: T.number.isRequired,
  onUpdateIndex: T.func.isRequired
}


export default CarouselThumbnails
