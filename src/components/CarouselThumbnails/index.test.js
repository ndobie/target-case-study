import { shallow } from 'enzyme'
import React from 'react'

import CarouselThumbnails from './index'
import Image from '../Image'
import Button from '../Button'

const testImages = ['a', 'b', 'c', 'd']

describe('CarouselThumbnails', () => {
  it('should render with minimal properties', () => {
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={0} onUpdateIndex={() => {}} />)
    expect(wrapper).toBeDefined()
    expect(wrapper.exists()).toBe(true)
  })

  it('should have the correct image order', () => {
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={0} onUpdateIndex={() => {}} />)

    const images = wrapper.find(Image)

    expect(images.at(0).prop('src')).toBe('d')
    expect(images.at(1).prop('src')).toBe('a')
    expect(images.at(2).prop('src')).toBe('b')
  })

  it('should outline the correct image', () => {
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={0} onUpdateIndex={() => {}} />)

    const images = wrapper.find(Image)

    expect(images.at(1).prop('src')).toBe('a')
    expect(images.at(1).prop('outline')).toBe(true)
  })

  it('clicking previous should return previous index', () => {
    const mock = jest.fn()
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={1} onUpdateIndex={mock} />)

    const button = wrapper.find(Button).at(0)
    button.simulate('click')
    expect(mock).toHaveBeenCalled()
    expect(mock).toHaveBeenCalledWith(0)
  })

  it('clicking previous should return last index on first image', () => {
    const mock = jest.fn()
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={0} onUpdateIndex={mock} />)

    const button = wrapper.find(Button).at(0)
    button.simulate('click')
    expect(mock).toHaveBeenCalled()
    expect(mock).toHaveBeenCalledWith(3)
  })

  it('clicking previous image should return previous index', () => {
    const mock = jest.fn()
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={1} onUpdateIndex={mock} />)

    const image = wrapper.find(Image).at(0).parent()
    image.simulate('click')
    expect(mock).toHaveBeenCalled()
    expect(mock).toHaveBeenCalledWith(0)
  })

  it('clicking next should return next index', () => {
    const mock = jest.fn()
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={1} onUpdateIndex={mock} />)

    const button = wrapper.find(Button).at(1)
    button.simulate('click')
    expect(mock).toHaveBeenCalled()
    expect(mock).toHaveBeenCalledWith(2)
  })

  it('clicking next should return first index on last image', () => {
    const mock = jest.fn()
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={3} onUpdateIndex={mock} />)

    const button = wrapper.find(Button).at(1)
    button.simulate('click')
    expect(mock).toHaveBeenCalled()
    expect(mock).toHaveBeenCalledWith(0)
  })

  it('clicking next image should return next index', () => {
    const mock = jest.fn()
    const wrapper = shallow(<CarouselThumbnails images={testImages} activeIndex={1} onUpdateIndex={mock} />)

    const image = wrapper.find(Image).at(2).parent()
    image.simulate('click')
    expect(mock).toHaveBeenCalled()
    expect(mock).toHaveBeenCalledWith(2)
  })
})
