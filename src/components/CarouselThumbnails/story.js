import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import CarouselThumbnails from './index'

const images = [
  'http://target.scene7.com/is/image/Target/14263758',
  'http://target.scene7.com/is/image/Target/14263758_Alt01',
  'http://target.scene7.com/is/image/Target/14263758_Alt02',
  'http://target.scene7.com/is/image/Target/14263758_Alt03',
  'http://target.scene7.com/is/image/Target/14263758_Alt04',
  'http://target.scene7.com/is/image/Target/14263758_Alt05',
  'http://target.scene7.com/is/image/Target/14263758_Alt06',
  'http://target.scene7.com/is/image/Target/14263758_Alt07'
]

storiesOf('element.CarouselThumbnails', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 500
  }))
  .add('first image', () => (
    <CarouselThumbnails
      images={images}
      activeIndex={0}
      onUpdateIndex={action('update index')} />
  ))
  .add('middle image', () => (
    <CarouselThumbnails
      images={images}
      activeIndex={3}
      onUpdateIndex={action('update index')} />
  ))
  .add('last image', () => (
    <CarouselThumbnails
      images={images}
      activeIndex={7}
      onUpdateIndex={action('update index')} />
  ))
