import React from 'react'
import T from 'prop-types'
import { css } from 'glamor'
import omit from 'lodash.omit';

export const H1 = 'h1'
export const H2 = 'h2'
export const H3 = 'h3'

const baseStyles = css({
  margin: 0,
  fontWeight: 'normal',
  lineHeight: '1.5'
})

const styles = {
  [H1]: css({fontSize: '2rem'}),
  [H2]: css({fontSize: '1.5rem'}),
  [H3]: css({fontSize: '1.25rem'})
}

const Heading = (props) => {
  const {children, type: Type} = props;
  const otherProps = omit(props, ['children', 'type']);
  return <Type {...otherProps} {...css(baseStyles, styles[Type])}>{children}</Type>
}

Heading.propTypes = {
  children: T.node.isRequired,
  type: T.oneOf([H1, H2, H3])
}

Heading.defaultProps = {
  type: H1
}

export default Heading;
