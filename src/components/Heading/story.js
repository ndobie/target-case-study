import { storiesOf, action } from '@kadira/storybook'
import React, { Component } from 'react'
import { host } from 'storybook-host'
import { withKnobs, text, boolean, select } from '@kadira/storybook-addon-knobs';

import Heading, {H1, H2, H3} from './index'

const headingTypes = {
  [H1]: 'Heading One',
  [H2]: 'Heading Two',
  [H3]: 'Heading Three'
}

storiesOf('elements.Heading', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 500
  }))
  .addDecorator(withKnobs)
  .add('normal heading', () => (
    <Heading>Hello World</Heading>
  ))
  .add('heading two', () => (
    <Heading type={H2}>Hello World</Heading>
  ))
  .add('heading center', () => (
    <Heading type={H2} center>Hello World</Heading>
  ))
  .add('playground', () => (
    <Heading
      type={select('Heading Type', headingTypes, H1)}
      center={boolean('Center Text', false)}>
        {text('Content', 'Hello World')}
    </Heading>
  ))
