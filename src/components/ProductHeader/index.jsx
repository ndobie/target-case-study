import React from 'react'
import T from 'prop-types'
import { css, media, presets } from 'glamor'

import Carousel from '../Carousel'
import Heading, {H1} from '../Heading'

const sectionStyles = css(
  media(presets.desktop, {
    width: '50%'
  })
)

const headingStyles = css({
  textAlign: 'center',
  padding: '0 2rem 1rem'
})

const ProductHeader = ({title, images, activeImageIndex, onViewLargerImage, onUpdateActiveImage}) => (
  <section {...sectionStyles}>
    <Heading {...headingStyles} type={H1}>{title}</Heading>
    <Carousel
      images={images}
      activeIndex={activeImageIndex}
      onViewLarger={onViewLargerImage}
      onUpdateIndex={onUpdateActiveImage} />
  </section>
)

export default ProductHeader
