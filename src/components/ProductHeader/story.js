import { storiesOf, action } from '@kadira/storybook'
import React from 'react'

import ProductHeader from './index'

const images = [
  'http://target.scene7.com/is/image/Target/14263758',
  'http://target.scene7.com/is/image/Target/14263758_Alt01',
  'http://target.scene7.com/is/image/Target/14263758_Alt02',
  'http://target.scene7.com/is/image/Target/14263758_Alt03',
  'http://target.scene7.com/is/image/Target/14263758_Alt04',
  'http://target.scene7.com/is/image/Target/14263758_Alt05',
  'http://target.scene7.com/is/image/Target/14263758_Alt06',
  'http://target.scene7.com/is/image/Target/14263758_Alt07'
]

storiesOf('section.ProductHeader', module)
  .add('default', () => (
    <ProductHeader
      title='Ninja™ Professional Blender with Single Serve Blending Cups'
      images={images}
      activeImageIndex={0}
      onViewLargerImage={action('view larger image')}
      onUpdateActiveImage={action('update active image')} />
  ))
