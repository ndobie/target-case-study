import { storiesOf } from '@kadira/storybook'
import React from 'react'

import Flex from './index'

const Placeholder = ({ children }) => (<div style={{padding: '1rem', backgroundColor: '#CB0000', textAlign: 'center', color: 'white', fontWeight: 'bold', whiteSpace: 'nowrap'}}>{children}</div>)


storiesOf('elements.Flex', module)
  .add('two columns', () => (
    <Flex container>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
    </Flex>
  ))
  .add('three columns', () => (
    <Flex container>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
    </Flex>
  ))
  .add('two-thirds columns', () => (
    <Flex container>
      <Flex columns={2}>
        <Placeholder>2</Placeholder>
      </Flex>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
    </Flex>
  ))
  .add('nested containers', () => (
    <Flex container>
      <Flex container>
        <Flex>
          <Placeholder>1</Placeholder>
        </Flex>
        <Flex>
          <Placeholder>1</Placeholder>
        </Flex>
        <Flex>
          <Placeholder>1</Placeholder>
        </Flex>
      </Flex>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
    </Flex>
  ))
  .add('fit contents', () => (
    <Flex container>
      <Flex columns={0}>
        <Placeholder>Fit Contents</Placeholder>
      </Flex>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
    </Flex>
  ))
  .add('collapse columns', () => (
    <Flex container>
      <Flex collapse>
        <Placeholder>1</Placeholder>
      </Flex>
      <Flex collapse>
        <Placeholder>1</Placeholder>
      </Flex>
      <Flex collapse>
        <Placeholder>1</Placeholder>
      </Flex>
    </Flex>
  ))
  .add('blank column', () => (
    <Flex container>
      <Flex>
        <Placeholder>1</Placeholder>
      </Flex>
      <Flex />
    </Flex>
  ))
