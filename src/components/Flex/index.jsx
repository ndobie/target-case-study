import React from 'react'
import T from 'prop-types'
import { css, media, presets, style, lastChild } from 'glamor'

const Flex = ({children, columns, container, collapse, align}) => {
  const styles = css(
    style({
      display: container ? 'flex' : 'block',
      justifyContent: 'stretch',
      marginRight: collapse ? 0 : '1rem',
      alignItems: align
    }),
    style({
      flexGrow: columns,
      flexShrink: 0,
      flexBasis: 1
    }),
    lastChild({
      marginRight: 0
    })
  )

  return <div {...styles}>{children}</div>
}

function getFlexSize (columns) {
  return style({flexGrow: columns, flexShrink: 0, flexBasis: 1})
}

Flex.propTypes = {
  children: T.node,
  columns: T.number,
  container: T.bool,
  collapse: T.bool
}

Flex.defaultProps = {
  columns: 1,
  container: false,
  collapse: false
}

export default Flex
