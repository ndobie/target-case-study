import React from 'react'
import T from 'prop-types'
import { css, media, presets } from 'glamor'

import Review from '../Review'
import Heading, {H2} from '../Heading'
import Flex from '../Flex'
import Stars from '../Stars'

const sectionStyles = css(
  media(presets.desktop, {
    width: '50%'
  })
)

const overviewStyles = css({
  fontSize: '1.05rem',
  fontWeight: 'bold',
  padding: '1rem'
})

const containerStyles = css({
  backgroundColor: '#F2F2F2',
  padding: '1rem'
})

const subtitleStyles = css({
  color: '#999',
  fontSize: '0.8rem',
  borderBottom: '1px solid #999',
  marginTop: 0,
  paddingTop: 0,
  marginBottom: '1rem',
  paddingBottom: '1rem'
})

const ReviewsSummary = ({overallStars, totalReviews, bestProReview, bestConReview}) => (
  <section {...sectionStyles}>
    <div {...overviewStyles}>
      <Flex container align='flex-end'>
        <Flex>
          <Stars num={overallStars} large />&nbsp;
          <span>overall</span>
        </Flex>
        <Flex>
          <div style={{textAlign: 'right'}}>view all {totalReviews} reviews</div>
        </Flex>
      </Flex>
    </div>
    <div {...containerStyles}>
      <Flex container>
        <Flex collapse>
          <Heading type={H2}>PRO</Heading>
          <p {...subtitleStyles}>most helpful 4-5 review</p>
          <Review {...bestProReview} />
        </Flex>
        <Flex collapse>
          <Heading type={H2}>CON</Heading>
          <p {...subtitleStyles}>most helpful 1-2 review</p>
          <Review {...bestConReview} />
        </Flex>
      </Flex>
    </div>
  </section>
)

export default ReviewsSummary
