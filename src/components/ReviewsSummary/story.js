import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import ReviewsSummary from './index'

const bestProReview = {
  stars: 5,
  shortDescription: 'Fantastic Blender',
  longDescription: 'This blender works amazingly, and blends within seconds.  The single serve cups also work really well for smoothies or protein shakes!',
  date: new Date(2013, 3, 18),
  authorName: 'Eric',
  authorLink: 'http://target.com'
}

const bestConReview = {
  stars: 1,
  shortDescription: 'Very unhappy',
  longDescription: 'Less than 2 months after purchase it completely stopped working. First it wouldn\'t detect the pitcher when trying to blend a significant amount, a couple weeks later it wouldn\'t detect the single serve cup.',
  date: new Date(2013, 2, 11),
  authorName: 'New York',
  authorLink: 'http://target.com'
}

storiesOf('section.ReviewsSummary', module)
  .add('default', () => (
    <ReviewsSummary
      overallStars={5}
      totalReviews={14}
      bestProReview={bestProReview}
      bestConReview={bestConReview}
      />
  ))
