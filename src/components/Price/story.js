import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import Price from './index'

storiesOf('elements.Price', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false
  }))
  .add('default', () => (
    <Price price={{ formattedPriceValue: '$10.99', priceQualifier: 'Online Price' }} />
  ))
