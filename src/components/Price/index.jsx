import React from 'react'
import T from 'prop-types'
import { css } from 'glamor'

const priceStyles = css({
  fontSize: '1.5rem',
  fontWeight: 'bold',
  marginRight: '0.25rem'
})

const labelStyles = css({
  fontSize: '0.75rem',
  color: '#999',
  textTransform: 'lowercase'
})

const Price = ({price}) => (
  <div>
    <span {...priceStyles}>{price.formattedPriceValue}</span>
    <span {...labelStyles}>{price.priceQualifier}</span>
  </div>
)

Price.propTypes = {
  price: T.shape({
    formattedPriceValue: T.string.isRequired,
    priceQualifier: T.string.isRequired
  }).isRequired
}

export default Price
