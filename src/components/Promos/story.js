import { storiesOf, action } from '@kadira/storybook'
import React, { Component } from 'react'
import { host } from 'storybook-host'

import Promos from './index'

const promos = [
  {
    Description: [{
      shortDescription: 'Free Gift Card'
    }]
  },
  {
    Description: [{
      shortDescription: 'Free Shipping'
    }]
  }
];

storiesOf('elements.Promos', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 600
  }))
  .add('default', () => (
    <Promos promoItems={promos} />
  ))
