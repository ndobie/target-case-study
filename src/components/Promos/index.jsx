import React from 'react'
import T from 'prop-types'
import { css, lastChild } from 'glamor'

const promosStyles = css({
  paddingBottom: '1rem',
  paddingTop: '1rem',
  borderBottom: '1px #CCC solid',
  borderTop: '1px #CCC solid'
})

const promoStyles = css(
  {
    marginBottom: '0.5rem',
    color: '#CB0000'
  },
  lastChild({
    marginBottom: 0
  })
)

const Promos = ({promoItems}) => (
  <div {...promosStyles}>
    {promoItems.map((promo) => (
      <div {...promoStyles}><i className="icon-">price_tag</i> {promo.Description[0].shortDescription}</div>
    ))}
  </div>
)

export default Promos
