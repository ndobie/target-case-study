import React from 'react'
import T from 'prop-types'
import { css, style, select} from 'glamor'

import Button, { ROUND } from '../Button'
import Flex from '../Flex'

const counterWrapperStyle = css({
  borderColor: '#CCCCCC',
  borderStyle: 'solid',
  borderWidth: 1,
  borderRadius: 3,
  padding: '0.5rem'
})

const labelStyle = css({
  textTransform: 'lowercase',
  fontSize: '0.8rem'
})

const countStyle = css({
  width: 40,
  textAlign: 'center',
  marginLeft: '0.5rem',
  marginRight: '0.5rem'
})

const Counter = ({label, value, onChange}) => (
  <div {...counterWrapperStyle}>
    <Flex container align='center'>
      <Flex collapse>
        <label {...labelStyle}>{label}</label>
      </Flex>
      <Flex columns={0} collapse>
        <Button type={ROUND} dark useIcons onClick={() => onChange(value - 1)}>minus</Button>
      </Flex>
      <Flex columns={0} collapse>
        <div {...countStyle}>{value}</div>
      </Flex>
      <Flex columns={0} collapse>
        <Button type={ROUND} dark useIcons onClick={() => onChange(value + 1)}>plus</Button>
      </Flex>
    </Flex>
  </div>
)

Counter.propTypes = {
  label: T.string.isRequired,
  value: T.number.isRequired,
  onChange: T.func.isRequired
}

export default Counter
