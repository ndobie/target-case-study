import { storiesOf, action } from '@kadira/storybook'
import React, { Component } from 'react'
import { host } from 'storybook-host'

import Counter from './index'

storiesOf('elements.Counter', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 300
  }))
  .add('default', () => (
    <Counter label='Quanity:' value={1} min={1} max={10} onChange={action('counter changed')} />
  ))
