import { shallow } from 'enzyme'
import React from 'react'

import Counter from './index'
import Button from '../Button'

describe('Counter', () => {
  it('should render with minimal properties', () => {
    const wrapper = shallow(<Counter label='test' value={1} onChange={() => {}} />)
    expect(wrapper).toBeDefined()
    expect(wrapper.exists()).toBe(true)
  })

  it('should show the label', () => {
    const wrapper = shallow(<Counter label='test' value={1} onChange={() => {}} />)
    expect(wrapper.contains('test')).toBe(true)
  })

  it('should show the quanity', () => {
    const wrapper = shallow(<Counter label='test' value={1} onChange={() => {}} />)
    expect(wrapper.contains(1)).toBe(true)
  })

  it('should give previous quanity on subtract', () => {
    const mock = jest.fn()
    const wrapper = shallow(<Counter label='test' value={1} onChange={mock} />)
    const button = wrapper.find(Button).at(0)
    button.simulate('click')
    expect(mock).toHaveBeenCalled();
    expect(mock).toHaveBeenCalledWith(0);
  })

  it('should give next quanity on add', () => {
    const mock = jest.fn()
    const wrapper = shallow(<Counter label='test' value={1} onChange={mock} />)
    const button = wrapper.find(Button).at(1)
    button.simulate('click')
    expect(mock).toHaveBeenCalled();
    expect(mock).toHaveBeenCalledWith(2);
  })
})
