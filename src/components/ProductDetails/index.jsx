import React, { Component } from 'react'
import T from 'prop-types'
import { css, lastChild, media, presets } from 'glamor'

import Counter from '../Counter'
import Flex from '../Flex'
import Heading, {H2} from '../Heading'
import List from '../List'
import Price from '../Price'
import PrimaryActionGroup from '../PrimaryActionGroup'
import Promos from '../Promos'
import ReturnPolicy from '../ReturnPolicy'
import SubActionGroup from '../SubActionGroup'

const sectionStyles = css(
  {
    padding: '2rem'
  },
  media(presets.desktop, {
    position: 'absolute',
    top: 0,
    right: 0,
    width: '50%',
    boxSizing: 'border-box'
  })
)

const itemStyles = css(
  {
    marginBottom: '2rem'
  },
  lastChild({
    marginBottom: 0
  })
)

const ProductDetails = ({price, promoItems, quanity, onUpdateQuanity, purchaseCode, onAddToCart, onFindInAStore, onPickUpInStore, onAddToRegistry, onAddToList, onShare, productHighlights}) => (
  <section {...sectionStyles}>
    <div {...itemStyles}><Price price={price} /></div>
    {promoItems && <div {...itemStyles}><Promos promoItems={promoItems} /></div> }
    <div {...itemStyles}>
      <Flex container>
        <Flex><Counter label='Quanity' value={quanity} onChange={onUpdateQuanity} /></Flex>
        <Flex/>
      </Flex>
    </div>
    <div {...itemStyles}><PrimaryActionGroup purchaseCode={purchaseCode} onAddToCart={onAddToCart} onFindInAStore={onFindInAStore} onPickUpInStore={onPickUpInStore} /></div>
    <div {...itemStyles}><ReturnPolicy /></div>
    <div {...itemStyles}><SubActionGroup onAddToRegistry={onAddToRegistry} onAddToList={onAddToList} onShare={onShare} /></div>
    <div {...itemStyles}>
      <Heading type={H2}>product highlights</Heading>
      <List items={productHighlights} />
    </div>
  </section>
)

export default ProductDetails
