import { storiesOf, action } from '@kadira/storybook'
import React from 'react'

import ProductDetails from './index'

const promoItems = [
 {
  "Description": [
   {
    "legalDisclaimer": "Offer available online only. Offer applies to purchases of $50 or more of eligible items across all categories. Look for the &quot;SPEND $50: SHIPS FREE&quot; logo on eligible items. Some exclusions apply. Items that are not eligible are subject to shipping charges. $50 purchase is based on eligible merchandise subtotal. Items that are not eligible, GiftCards, e-GiftCards, gift wrap, tax and shipping and handling charges will not be included in determining merchandise subtotal. Offer valid for orders shipping within the 48 contiguous states, as well as APO\/FPO and for Standard and To the Door shipping methods only. Not valid on previous orders.",
    "shortDescription": "SPEND $50, GET FREE SHIPPING"
   }
  ],
  "endDate": "2014-05-25 06:59:00.001",
  "promotionIdentifier": "10736506",
  "promotionType": "Buy catalog entries from category X, get shipping at a fixed price",
  "startDate": "2014-05-18 07:00:00.001"
 },
 {
  "Description": [
   {
    "legalDisclaimer": "Receive a $25 gift card when you buy a Ninja Professional Blender with single serve blending cups or a Ninja MEGA Kitchen System. Not valid on previous orders. On your order summary, the item subtotal will reflect the price of the qualifying item plus the amount of the free gift card, followed by a discount given for the amount of the free gift card. &nbsp;Your price on the order summary will be the price of the qualifying item (the total charges for the qualifying item and gift card). &nbsp;Your account will actually be charged the amount of the qualifying item reduced by the amount of the gift card, and a separate charge for the amount of the gift card. The gift card will be sent to the same address as your order and will ship separately. If you want to return the item you purchased to a Target Store, you may either keep the gift card and just return the qualifying item (you will be refunded the amount of the qualifying item reduced by the amount of the gift card), or you can return the qualifying item and the gift card &nbsp;for a full refund using the online receipt. If you return the item you purchased by mail, keep the gift card; you will be refunded the amount of the qualifying item reduced by the amount of the gift card. Offer expires 05\/24\/14 at 11:59pm PST.",
    "shortDescription": "$25 gift card with purchase of a select Ninja Blender"
   }
  ],
  "endDate": "2014-05-25 06:59:00.001",
  "promotionIdentifier": "10730501",
  "promotionType": "Multiple Items Free Gift",
  "startDate": "2014-05-11 07:00:00.001"
 }
];

const highlights = [
 "<strong>Wattage Output:<\/strong> 1100 Watts",
 "<strong>Number of Speeds:<\/strong> 3 ",
 "<strong>Capacity (volume):<\/strong> 72.0 Oz.",
 "<strong>Appliance Capabilities:<\/strong> Blends",
 "<strong>Includes:<\/strong> Travel Lid",
 "<strong>Material:<\/strong> Plastic",
 "<strong>Finish:<\/strong> Painted",
 "<strong>Metal Finish:<\/strong> Chrome",
 "<strong>Safety and Security Features:<\/strong> Non-Slip Base",
 "<strong>Care and Cleaning:<\/strong> Easy-To-Clean, Dishwasher Safe Parts"
];

storiesOf('section.ProductDetails', module)
  .add('default', () => (
    <ProductDetails
      price={{ formattedPriceValue: '$10.99', priceQualifier: 'Online Price' }}
      productHighlights={highlights}
      quanity={5}
      onUpdateQuanity={action('update quanity')} />
  ))
  .add('promos', () => (
    <ProductDetails
      price={{ formattedPriceValue: '$10.99', priceQualifier: 'Online Price' }} 
      promoItems={promoItems}
      productHighlights={highlights}
      quanity={5}
      onUpdateQuanity={action('update quanity')} />
  ))
