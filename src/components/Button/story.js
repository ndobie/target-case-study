import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'
import { withKnobs, text, boolean, select } from '@kadira/storybook-addon-knobs';


import Button, { DEFAULT, LINK, ROUND, SECONDARY, PRIMARY } from './index'

const styleTypes = {
  [DEFAULT]: 'Default',
  [LINK]: 'Link',
  [ROUND]: 'Round',
  [SECONDARY]: 'Secondary',
  [PRIMARY]: 'Primary',
}

storiesOf('elements.Button', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 400
  }))
  .addDecorator(withKnobs)
  .add('with text', () => (<Button onClick={action('clicked')}>Hello World</Button>))
  .add('with icon and text', () => (<Button type={PRIMARY} large onClick={action('clicked')}><span className="icon-">price_tag</span> Add to Cart</Button>))
  .add('with rounded style', () => (<Button type={ROUND} useIcons onClick={action('clicked')}>plus</Button>))
  .add('with dark style', () => (<Button dark onClick={action('clicked')}>I am dark</Button>))
  .add('playground',
    () => (
      <Button
        onClick={action('clicked')}
        active={boolean('Is Active', false)}
        focused={boolean('Is Focused', false)}
        large={boolean('Large Size', false)}
        dark={boolean('Dark Style', false)}
        useIcons={boolean('Use Icons', false)}
        type={select('Style', styleTypes, DEFAULT)}>
          {text('Content', 'Hello World')}
      </Button>
    )
  )
