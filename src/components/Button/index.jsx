import classnames from 'classnames'
import T from 'prop-types'
import React from 'react'

import styles from './index.css'

export const DEFAULT = 'default'
export const LINK = 'link'
export const ROUND = 'round'
export const SECONDARY = 'secondary'
export const PRIMARY = 'primary'

const Button = ({ children, onClick, type, large, dark, focused, active, useIcons }) => {
  const className = classnames(
    'Button',
    {
      'Button--large': large,
      'Button--dark': dark,
      'Button--link': type === LINK,
      'Button--round': type === ROUND,
      'Button--secondary': type === SECONDARY,
      'Button--primary': type === PRIMARY,
      'is-focused': focused && !active,
      'is-active': active
    }
  )

  return <button className={className} onClick={onClick}>{useIcons ? <span className="icon-">{children}</span> : children}</button>
}

Button.propTypes = {
  type: T.oneOf([DEFAULT, LINK, ROUND, SECONDARY]),
  children: T.node.isRequired,
  onClick: T.func.isRequired,
  focused: T.bool,
  active: T.bool,
  large: T.bool,
  useIcons: T.bool,
  dark: T.bool
}

Button.defaultProps = {
  type: DEFAULT,
  focused: false,
  active: false,
  large: false,
  dark: false,
  useIcons: false
}

export default Button
