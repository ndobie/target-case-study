import React from 'react'
import T from 'prop-types'
import { css, media, presets } from 'glamor'

import Button, {LINK} from '../Button'
import CarouselThumbnails from '../CarouselThumbnails'
import Image from '../Image'

const imagePositioningContainerStylers = css({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: '100%'
})

const imageSizingContainerStyles = css(
  {
    width: '100vmin',
    height: '100vmin',
    maxWidth: 600,
    maxHeight: 600
  },
  media(presets.desktop, {
    width: '50vmin',
    height: '50vmin'
  })
)

const Carousel = ({images, activeIndex, onUpdateIndex, onViewLarger}) => (
  <div>
    <div {...imagePositioningContainerStylers}>
      <div {...imageSizingContainerStyles}>
        <Image src={images[activeIndex]} width='100%' height='100%' />
      </div>
    </div>
    <Button large type={LINK} onClick={() => onViewLarger(activeIndex)}><i className="icon-">zoom_in</i>&nbsp;view larger</Button>
    <div style={{height: '1rem'}} />
    <CarouselThumbnails images={images} activeIndex={activeIndex} onUpdateIndex={onUpdateIndex} />
  </div>
)

export default Carousel
