import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import Carousel from './index'

const images = [
  'http://target.scene7.com/is/image/Target/14263758',
  'http://target.scene7.com/is/image/Target/14263758_Alt01',
  'http://target.scene7.com/is/image/Target/14263758_Alt02',
  'http://target.scene7.com/is/image/Target/14263758_Alt03',
  'http://target.scene7.com/is/image/Target/14263758_Alt04',
  'http://target.scene7.com/is/image/Target/14263758_Alt05',
  'http://target.scene7.com/is/image/Target/14263758_Alt06',
  'http://target.scene7.com/is/image/Target/14263758_Alt07'
]

storiesOf('modules.Carousel', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 700
  }))
  .add('default', () => (
    <Carousel
      images={images}
      activeIndex={1}
      onViewLarger={action('view larger')}
      onUpdateIndex={action('update index')} />
  ))
