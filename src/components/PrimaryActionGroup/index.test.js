import { shallow } from 'enzyme'
import React from 'react'

import PrimaryActionGroup, {AVAILABLE_STORE_ONLINE, AVAILABLE_ONLINE, AVAILABLE_STORE} from './index'
import Button, {PRIMARY, SECONDARY} from '../Button'

describe('PrimaryActionGroup', () => {
  it('should render with minimal properties', () => {
    const wrapper = shallow(<PrimaryActionGroup purchaseCode={AVAILABLE_STORE_ONLINE} onAddToCart={() => {}} onFindInAStore={() => {}} onPickUpInStore={() => {}} />)
    expect(wrapper).toBeDefined()
    expect(wrapper.exists()).toBe(true)
  })

  it('should render all the buttons when available online and in stores', () => {
    const wrapper = shallow(<PrimaryActionGroup purchaseCode={AVAILABLE_STORE_ONLINE} onAddToCart={() => {}} onFindInAStore={() => {}} onPickUpInStore={() => {}} />)
    expect(wrapper.find(Button).length).toBe(3)
  })

  it('should render the in store buttons when available only in stores', () => {
    const wrapper = shallow(<PrimaryActionGroup purchaseCode={AVAILABLE_STORE} onAddToCart={() => {}} onFindInAStore={() => {}} onPickUpInStore={() => {}} />)
    expect(wrapper.find(Button).length).toBe(2)
    expect(wrapper.contains('Pick Up in Store')).toBe(true)
    expect(wrapper.contains('Find in a Store')).toBe(true)
  })

  it('should render the online button when available only online', () => {
    const wrapper = shallow(<PrimaryActionGroup purchaseCode={AVAILABLE_ONLINE} onAddToCart={() => {}} onFindInAStore={() => {}} onPickUpInStore={() => {}} />)
    expect(wrapper.find(Button).length).toBe(1)
    expect(wrapper.contains('Add to Cart')).toBe(true)
  })

  it('should trigger action when add to cart is clicked', () => {
    const atcMock = jest.fn()
    const puisMock = jest.fn()
    const fiasMock = jest.fn()

    const wrapper = shallow(<PrimaryActionGroup purchaseCode={AVAILABLE_STORE_ONLINE} onAddToCart={atcMock} onFindInAStore={fiasMock} onPickUpInStore={puisMock} />)

    const button = wrapper.find(Button).at(2)
    button.simulate('click')
    expect(atcMock).toHaveBeenCalled()
  })

  it('should trigger action when pick up in store is clicked', () => {
    const atcMock = jest.fn()
    const puisMock = jest.fn()
    const fiasMock = jest.fn()

    const wrapper = shallow(<PrimaryActionGroup purchaseCode={AVAILABLE_STORE_ONLINE} onAddToCart={atcMock} onFindInAStore={fiasMock} onPickUpInStore={puisMock} />)

    const button = wrapper.find(Button).at(0)
    button.simulate('click')
    expect(puisMock).toHaveBeenCalled()
  })

  it('should trigger action when find in a store is clicked', () => {
    const atcMock = jest.fn()
    const puisMock = jest.fn()
    const fiasMock = jest.fn()

    const wrapper = shallow(<PrimaryActionGroup purchaseCode={AVAILABLE_STORE_ONLINE} onAddToCart={atcMock} onFindInAStore={fiasMock} onPickUpInStore={puisMock} />)

    const button = wrapper.find(Button).at(1)
    button.simulate('click')
    expect(fiasMock).toHaveBeenCalled()
  })
})
