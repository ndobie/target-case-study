import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import PrimaryActionGroup, { AVAILABLE_STORE_ONLINE } from './index'

storiesOf('modules.PrimaryActionGroup', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 600
  }))
  .add('default', () => (
    <PrimaryActionGroup
      purchaseCode={AVAILABLE_STORE_ONLINE}
      onPickUpInStore={action('pick up in store')}
      onAddToCart={action('add to cart')}
      onFindInAStore={action('find in a store')} />
  ))
