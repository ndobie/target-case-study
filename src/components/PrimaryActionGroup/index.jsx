import React from 'react'
import T from 'prop-types'

import Button, { LINK, PRIMARY, SECONDARY } from '../Button'
import Flex from '../Flex'

export const AVAILABLE_STORE_ONLINE = 0
export const AVAILABLE_ONLINE = 1
export const AVAILABLE_STORE = 2

const PrimaryActionGroup = ({ purchaseCode, onPickUpInStore, onFindInAStore, onAddToCart }) => (
  <Flex container collapse>
    {(purchaseCode === AVAILABLE_STORE_ONLINE || purchaseCode === AVAILABLE_STORE) && <Flex>
      <Button type={SECONDARY} large onClick={onPickUpInStore}>Pick Up in Store</Button>
      <Button type={LINK} onClick={onFindInAStore}>
        <span style={{textTransform: 'lowercase'}}>Find in a Store</span>
      </Button>
    </Flex>}
    {(purchaseCode === AVAILABLE_STORE_ONLINE || purchaseCode === AVAILABLE_ONLINE) && <Flex>
      <Button type={PRIMARY} large onClick={onAddToCart}>Add to Cart</Button>
    </Flex>}
    {purchaseCode !== AVAILABLE_STORE_ONLINE && <Flex />}
  </Flex>
)

PrimaryActionGroup.propTypes = {
  purchaseCode: T.oneOf([AVAILABLE_STORE_ONLINE, AVAILABLE_ONLINE, AVAILABLE_STORE]).isRequired,
  onPickUpInStore: T.func.isRequired,
  onAddToCart: T.func.isRequired,
  onFindInAStore: T.func.isRequired
}

export default PrimaryActionGroup
