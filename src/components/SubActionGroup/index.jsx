import React from 'react'
import T from 'prop-types'

import Button from '../Button'
import Flex from '../Flex'

const SubActionGroup = ({ onAddToRegistry, onAddToList, onShare }) => (
  <Flex container collapse>
    <Flex>
      <Button onClick={onAddToRegistry}>Add to Registry</Button>
    </Flex>
    <Flex>
      <Button onClick={onAddToList}>Add to List</Button>
    </Flex>
    <Flex>
      <Button onClick={onShare}>Share</Button>
    </Flex>
  </Flex>
)

SubActionGroup.propTypes = {
  onAddToRegistry: T.func.isRequired,
  onAddToList: T.func.isRequired,
  onShare: T.func.isRequired
}

export default SubActionGroup
