import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import SubActionGroup from './index'

storiesOf('modules.SubActionGroup', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 600
  }))
  .add('default', () => (
    <SubActionGroup
      onAddToRegistry={action('add to registry')}
      onAddToList={action('add to list')}
      onShare={action('add to share')} />
  ))
