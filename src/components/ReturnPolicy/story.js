import { storiesOf, action } from '@kadira/storybook'
import React, { Component } from 'react'
import { host } from 'storybook-host'

import ReturnPolicy from './index'

storiesOf('modules.ReturnPolicy', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false,
    width: 600
  }))
  .add('default', () => (
    <ReturnPolicy />
  ))
