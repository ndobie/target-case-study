import React from 'react'
import T from 'prop-types'
import { css } from 'glamor'

import Flex from '../Flex'
import Heading, {H3} from '../Heading'

const headingStyles = css({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-start',
  borderRight: '1px #999 solid',
  color: '#999',
  height: '100%',
  paddingRight: '1rem'
});

const policyStyles = css({
  fontSize: '0.75rem',
  color: '#666'
});

const ReturnPolicy = () => (
  <Flex container align='stretch'>
    <Flex columns={0}>
      <Heading type={H3} {...headingStyles}>returns</Heading>
    </Flex>
    <Flex>
      <p {...policyStyles}>This item must be returned within 30 days of the ship date. See return policy for details. Prices, promotions, styles and availability may vary by store and online.</p>
    </Flex>
  </Flex>
)

export default ReturnPolicy
