import React from 'react'
import T from 'prop-types'
import { css, lastChild } from 'glamor'

const itemStyles = css(
  {
    fontSize: '0.75rem',
    color: '#666',
    marginBottom: '0.5rem'
  },
  lastChild({
    marginBottom: 0
  })
)

const List = ({items}) => (
  <ul>
    {items.map(item => <li {...itemStyles} dangerouslySetInnerHTML={{__html: item}} />)}
  </ul>
)

export default List
