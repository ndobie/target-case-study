import { storiesOf, action } from '@kadira/storybook'
import React from 'react'
import { host } from 'storybook-host'

import List from './index'

const items = [
 "<strong>Wattage Output:<\/strong> 1100 Watts",
 "<strong>Number of Speeds:<\/strong> 3 ",
 "<strong>Capacity (volume):<\/strong> 72.0 Oz.",
 "<strong>Appliance Capabilities:<\/strong> Blends",
 "<strong>Includes:<\/strong> Travel Lid",
 "<strong>Material:<\/strong> Plastic",
 "<strong>Finish:<\/strong> Painted",
 "<strong>Metal Finish:<\/strong> Chrome",
 "<strong>Safety and Security Features:<\/strong> Non-Slip Base",
 "<strong>Care and Cleaning:<\/strong> Easy-To-Clean, Dishwasher Safe Parts"
];

storiesOf('elements.List', module)
  .addDecorator(host({
    align: 'center middle',
    cropMarks: true,
    mobXDevTools: false
  }))
  .add('default', () => (
    <List items={items} />
  ))
