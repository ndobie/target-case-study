# Target Case Study

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Running Case Study

Running is pretty straight forward, just run `yarn install` and `yarn start` (works with NPM) and open your browsers to the displayed address.

### Storybook

This project uses React Storybook for component development, use `yarn storybook` or `npm run storybook` to view the storybook.

### Unit Tests

This project uses Jest for testing, use `yarn test` or `npm test` to run the test suite.

## Managing Releases

The entire build pipeline would be linked to a Git repository. There would be three primary branches; master, staging, and production. The master branch represents the current development state of the application and is where all completed new features will be merged. Staging is a last-check branch to identify any bugs and patch them before the code is deployed. Production is the current production code. When any of these branches are updated, a deploy is started.

### Rules for Primary Branches

1. No direct commits. All updates to primary branches must be updated by pull requests with at least one approval on the latest commit and pass linting and testing suites.
3. Features can only be merged into master.
4. Bug fixes can only be merged into master and staging. Bugs found in staging should be merged to both branches.
5. Production can only be merged with staging.

### Primary Branch Build Flows

#### Master

1. Run linting and testing suites.
2. Generated the static Storybook and deploy internally.
3. Tag as alpha build, ex. 1.32.3-alpha.[short commit hash]
4. Compile development build.
5. Deploy to CDN.
6. Push tag.

#### Staging

1. Run linting and testing suites.
2. Tag as beta build, ex. 1.32.3-beta.[short commit hash]
3. Compile production build.
4. Deploy to CDN.
5. Push tag.

#### Production

1. Run linting and testing suites.
2. Generated changelog from commit messages.
3. Tag as version, ex. 1.32.3
4. Compile production build.
5. Deploy to CDN.
6. Push tag.

### Versioning

By using [Conventional Commit messages](https://conventionalcommits.org) the appropriate version bump for the application can be automatically determined. This helps makes sure that proper versioning is maintained and tools can be written to help enforce and assist with Conventional Commit messages.

### Other Branches

While the three primary branches would be automatically deployed, it would be feasible to create an on-demand service that could compile other branches for the application to allow for easy sharing of work in progress.

1. Developer opens `https://[branch-name].app.domain.com` in their browser.
2. Microservice shallow copies the branch.
3. MS checks for an appropriate cached version. If cache is good, then it is served.
4. MS installs NPM packages and displays message to developer that it is building the branch.
5. MS runs liniting and testing suites and storing the results at `https://[branch-name].app.domain.com/__build` and flashing a message to the user with a summary.
6. MS makes a development build.
7. MS serves the built branch.
8. MS cleans up build tools.

## Notes from Nick

- I didn't have time to write a full test suite so I picked a few to give a sample of what the tests would look like.
- I built this case study using Component Driven Development.
- Carousel and Counter are intended to be stateless and would be linked with something like Redux to manage state updating. This makes it easier to add new features that can update both, such as switching to a specific image or updating the quantity to a given amount.
- If I had more time users would be able to enter a number using a keyboard as well as using the buttons with Counter.
- Button used CSS instead of Glamor to save time.
- Depending on circumstances, I'd recommend creating separate packages for pieces that could be used with other projects, like UI controls. Using something like monorepo with [Lerna](https://lernajs.io) would make this process easier.
  - Some groups break down UIs into single component packages, eg. `@target/ui-button` and `@target/ui-counter`, this helps reduce including unused code.
- I used a three primary branch deploy structure, as in my opinion this allows for some planning with feature releases and time for more rigorous QA testing. Although a one, two, or four primary branch variation of this can work just as well, the main idea is that builds are tied to the branches updating an no manual deploy step is needed.
