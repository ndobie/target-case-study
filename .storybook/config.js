import { configure, setAddon, addDecorator } from '@kadira/storybook';
import infoAddon from '@kadira/react-storybook-addon-info';
import { setOptions } from '@kadira/storybook-addon-options';
import React from 'react'
import 'glamor/reset'

setAddon(infoAddon);

addDecorator((story) => (
  <div style={{padding: '1rem'}}>
    {story()}
  </div>
));

setOptions({
  name: 'TARGET CASE STUDY',
  url: 'https://gitlab.com/nrdobie/target-case-study',
  sortStoriesByKind: true,
});

function loadStories() {
  require('../src/components/Button/story')
  require('../src/components/Carousel/story')
  require('../src/components/CarouselThumbnails/story')
  require('../src/components/Counter/story')
  require('../src/components/Flex/story')
  require('../src/components/Heading/story')
  require('../src/components/Image/story')
  require('../src/components/List/story')
  require('../src/components/PrimaryActionGroup/story')
  require('../src/components/Price/story')
  require('../src/components/ProductDetails/story')
  require('../src/components/ProductHeader/story')
  require('../src/components/Promos/story')
  require('../src/components/ReturnPolicy/story')
  require('../src/components/Review/story')
  require('../src/components/ReviewsSummary/story')
  require('../src/components/Stars/story')
  require('../src/components/SubActionGroup/story')
}

configure(loadStories, module);
